﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Invoke("DestroyMe", .5f);

	}
	void DestroyMe()
    {

        Destroy(gameObject);

    }
	
}
