﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrenade : MonoBehaviour {


    public GameObject GrenadePrefab;
    public Transform hand;
    public float throwforce = 10f;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.G))
        {
            GameObject gren = Instantiate(GrenadePrefab, hand.position, hand.rotation) as GameObject;
            gren.GetComponent<Rigidbody>().AddForce(hand.forward * throwforce, ForceMode.Impulse);
        }
	}
}
